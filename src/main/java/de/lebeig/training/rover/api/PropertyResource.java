package de.lebeig.training.rover.api;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.lebeig.training.rover.service.PropertyService;

@RestController
@RequestMapping(path="/api/properties")
public class PropertyResource {

    @Autowired
    private PropertyService propertyService;


    @GetMapping("/hostname")
    public String getHostname() throws UnknownHostException {
        return propertyService.getHostname();
    }

}
