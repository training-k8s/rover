package de.lebeig.training.rover.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.lebeig.training.rover.domain.Rover;
import de.lebeig.training.rover.domain.RoverCommand;
import de.lebeig.training.rover.service.RoverService;

@RestController
@RequestMapping("/api/rover")
public class RoverRessource {

	@Autowired
	private RoverService roverService;

	@PostMapping("/{roverCommand}")
	@CrossOrigin
	public Rover executeCommand(@RequestBody Rover roverState, @PathVariable RoverCommand roverCommand) {
		return roverService.executeCommand(roverState, roverCommand);
	}
}
