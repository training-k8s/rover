package de.lebeig.training.rover.service;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import org.springframework.stereotype.Service;

@Service
public class PropertyService {


	public String getHostname() throws UnknownHostException {
		return Inet4Address.getLocalHost()
				.getHostName();
	}
}
