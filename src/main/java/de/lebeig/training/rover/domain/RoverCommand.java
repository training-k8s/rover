package de.lebeig.training.rover.domain;

public enum RoverCommand {
	ROTATE_LEFT,
	ROTATE_RIGHT,
	MOVE_FORWARD,
	MOVE_BACKWARD;
}
